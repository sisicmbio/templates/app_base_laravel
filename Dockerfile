FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sql
ENV APP_PROJETO "app___APP_NOME__"
COPY sql  /sql/
ENTRYPOINT ["/sbin/tini", "--","/bin/pdci_upd_sql.sh"]

#FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sislic
#ENV APP_PROJETO "app_lafsisbio"
#COPY ibama  /ibama/
#RUN chmod 777 /ibama/
#COPY sislic.sh /sislic.sh
#RUN chmod +x /sislic.sh

#ENTRYPOINT ["/sbin/tini", "--","/sislic.sh"]

#Tag Production
FROM registry.gitlab.com/sisicmbio/infra/co7_httpd_php_7.2:production  AS production
ARG PDCI_APP_PROJETO
ARG PDCI_COMMIT_REF_NAME
ARG PDCI_COMMIT_SHA
ARG PDCI_COMMIT_SHORT_SHA
ARG PDCI_COMMIT_TAG

RUN  rpm -Uvh https://yum.postgresql.org/10/redhat/rhel-7-x86_64/pgdg-centos10-10-2.noarch.rpm
RUN yum install --setopt=tsflags=nodocs postgresql10 postgis cronie -y \
 && yum clean all \
 && rm -rf /var/cache/yum

LABEL PDCI_COMMIT_REF_NAME="${PDCI_COMMIT_REF_NAME}"
LABEL PDCI_COMMIT_SHA="${PDCI_COMMIT_SHA}"
LABEL PDCI_COMMIT_SHORT_SHA="${PDCI_COMMIT_SHORT_SHA}"
LABEL PDCI_COMMIT_TAG="${PDCI_COMMIT_TAG}"

ENV APP_HOME /var/www/html/${PDCI_APP_PROJETO:-app___APP_NOME__}
ENV APP_PROJETO "${PDCI_APP_PROJETO:-app___APP_NOME__}"

RUN rm -rf /var/www/html/*
RUN mkdir -p $APP_HOME/
RUN rm -rf $APP_HOME/*


COPY conf/etc/httpd/conf.d/$APP_PROJETO.conf /etc/httpd/conf.d/$APP_PROJETO.conf
ADD run-httpd.sh /run-httpd.sh
RUN chmod -v +x /run-httpd.sh
COPY src/  $APP_HOME

COPY src/.env.example $APP_HOME/.env

RUN echo "date.timezone=\"America/Sao_Paulo\"" >> /etc/php.ini



WORKDIR $APP_HOME

RUN composer clearcache && \
    composer install --no-dev --optimize-autoloader

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

EXPOSE 80
#VOLUME ["/var/lib/php/session"]

CMD ["/run-httpd.sh"]

#Tag develop
FROM production AS develop

#Tag build
FROM develop AS build

#Tag testing
FROM registry.gitlab.com/sisicmbio/infra/co7_httpd_php_7.2:testing AS testing

ENV APP_HOME /var/www/html/${PDCI_APP_PROJETO:-app___APP_NOME__}
ENV APP_PROJETO "${PDCI_APP_PROJETO:-app___APP_NOME__}"

COPY conf/etc/httpd/conf.d/$APP_PROJETO.conf /etc/httpd/conf.d/$APP_PROJETO.conf
ENV APP_HOME /var/www/html

WORKDIR $APP_HOME

RUN rm -rf $APP_HOME/*

COPY src/  $APP_HOME

COPY src/.env.example $APP_HOME/.env

WORKDIR $APP_HOME

RUN composer clearcache && \
  composer install && \
  yarn install

RUN chown apache:apache $APP_HOME -R
RUN chmod 760 $APP_HOME -R

CMD ["/run-httpd.sh"]
